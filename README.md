### Notes

1. To run this, first make sure you have the `intel-mkl/2020.0.166` and `intel-compiler/2020.0.166` modules loaded.
2. Then run from the project home directory `cmake CMakeLists.txt` which will populate all the relevant directories
with build information. 
3. Then you can run `make` from the home directory and everything should be compiled into the ${HOME}/bin folder.

Here's what Trilinos was compiled with. 

```
#!/bin/bash

module load intel-mkl/2019.3.199
module load python3/3.7.4
module unload intel-mkl

module load intel-compiler/2019.5.281
module load intel-mkl/2019.5.281
module load openmpi/4.0.2
module load cuda/10.1
module load cmake/3.16.2

cmake \
-D CMAKE_INSTALL_PREFIX=/home/561/rw6126/trilinos/12.17 \
-D Trilinos_ENABLE_ML:BOOL=ON \
-D Trilinos_ENABLE_ALL_OPTIONAL_PACKAGES:BOOL=ON \
-D Trilinos_ENABLE_TESTS:BOOL=ON \
-D Trilinos_ENABLE_OpenMP:BOOL=ON \
-D DART_TESTING_TIMEOUT:STRING=600 \
-D TPL_ENABLE_MPI:BOOL=ON \
-D BLAS_LIBRARY_DIRS:STRING="$MKL/lib/intel64;$MKL/../compiler/lib/intel64" \
-D BLAS_LIBRARY_NAMES:STRING="mkl_intel_lp64; mkl_intel_thread; mkl_core; iomp5; pthread" \
-D LAPACK_LIBRARY_DIRS:STRING="$MKL/lib/intel64;$MKL/../compiler/lib/intel64" \
-D LAPACK_LIBRARY_NAMES:STRING="mkl_intel_lp64; mkl_intel_thread; mkl_core; iomp5; pthread" \
-D TPL_BLAS_LIBRARIES="$MKL/lib/intel64/libmkl_intel_lp64.so;$MKL/lib/intel64/libmkl_intel_thread.so;$MKL/lib/intel64/libmkl_core.so;$MKL/../compiler/lib/intel64/libiomp5.so" \
-D TPL_LAPACK_LIBRARIES="$MKL/lib/intel64/libmkl_intel_lp64.so;$MKL/lib/intel64/libmkl_intel_thread.so;$MKL/lib/intel64/libmkl_core.so;$MKL/../compiler/lib/intel64/libiomp5.so" \
..

find ./* -exec -i -e 's/qopenmp/fopenmp/g' {} \;
```

Note above that Trilinos was compiled with all packages enabled but Kokkos specific options such as Pthreads isn't
enabled meaning we can only get Kokkos to do serial work.

### KokkosTutorials directory

The KokkosTutorials directory contains some of the tutorials I've done. Copied and pasted out from [kokkos github](https://github.com/kokkos/kokkos-tutorials)
in the Exercises/ directory. KokkosTutorials/04.cpp line 100 is where we can start trying to use the other exec
spaces such as threads, openmp and cuda which doesn't work.o

### Misc Notes

- MPI is it's own library and is good to be run with without Kokkos or Teuchos or whatever.
- my default on gadi, we can run 48 copies of the same program with `mpirun  -np 48 [program]` without any complaints.\
  Does this mean that by default we have 48 cores to play with?
- I tried to do my own compilation and build but to no avail. For some reason enabling OpenMP gives us errors \
  while compiling but that might just be due to gadi systems being different to normal? PThreads option doesn't \
  seem to be available to be used either. CUDA compilation requires nvcc which we don't seem to have access to.
  UPDATE: fix is to run `find ./* -exec sed -i -e 's/qopenmp/fopenmp/g' {} /; in the /build directory for Trilinos
  to fix whatever the issue was with compiling with openmp.
