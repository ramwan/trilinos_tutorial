#include <Stratimikos_DefaultLinearSolverBuilder.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_LinearOpWithSolveFactoryHelpers.hpp>
#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Kokkos_Core.hpp>

typedef double Scalar;
typedef int LO;
typedef int GO;
typedef Tpetra::Vector<>::node_type NT;
typedef Tpetra::Map<LO, GO, NT> map_type;
typedef Tpetra::Vector<Scalar, LO, GO, NT> vector_type;
typedef Tpetra::CrsMatrix<Scalar, LO, GO, NT> matrix_type;
typedef Tpetra::Operator<Scalar, LO, GO, NT> op_type;

int main(int argc, char *argv[])
{
  using Teuchos::ArrayView;
  using Teuchos::ArrayRCP;
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  using Teuchos::tuple;
  typedef Teuchos::ParameterList::PrintOptions PLPrintOptions;
  
  Tpetra::ScopeGuard tpetracope(&argc, &argv);
  {
    auto comm = Tpetra::getDefaultComm();
    Teuchos::oblackholestream blackhole;
    std::ostream& out = (comm->getRank() == 0 ? std::cout : blackhole);
    RCP<std::basic_ostream<char> > stream = rcpFromRef(out);
    const RCP<Teuchos::FancyOStream> f_out = Teuchos::getFancyOStream(stream);

    Stratimikos::DefaultLinearSolverBuilder linearSolverBuilder;
    // This prints a whole load of stuff
    /*
    linearSolverBuilder.getValidParameters()->print(
      out, PLPrintOptions().indent(2).showTypes(true).showDoc(true));
    */

    RCP<Teuchos::ParameterList> p = Teuchos::parameterList();
    p->set("Linear Solver Type", "Belos");
    Teuchos::ParameterList& belosList =
      p->sublist("Linear Solver Types").sublist("Belos");
    belosList.set("Solver Type", "Pseudo Block GMRES");
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Maximum Iterations", 300);
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<int>("Num Blocks", 8);
    belosList.sublist("Solver Types").sublist("Pseudo Block GMRES")
      .set<double>("Convergence Tolerance", 1e-8);
    belosList.sublist("VerboseObject").set("Verbosity Level", "default");
    p->set("Preconditioner Type", "None");
    linearSolverBuilder.setParameterList(p);


    /* Create the Tpetra objects to be used */
    const GO total = 1000;
    const GO base = 0;
  
    RCP<map_type> map = rcp(new map_type(total, base, comm)); // contiguous map
    // an array of all the global elements on this process
    ArrayView<const GO> myGlobalElements = map->getNodeElementList();
    // number of entries in a specific row
    ArrayRCP<size_t> numNz = Teuchos::arcp<size_t>(total);

    // to be Kokkos-ified
    for(auto i=0; i<total; i++)
    {
      if(myGlobalElements[i] == 0 || myGlobalElements[i] == total-1)
        numNz[i] = 2;
      else
        numNz[i] = 3;
    }

    ArrayView<const size_t> numNzView =
      ArrayView<const size_t>(numNz.getRawPtr(), myGlobalElements.size());
    // create the finite difference matrix
    RCP<matrix_type> A = rcp(new matrix_type(map, numNzView));
    const Scalar two = 2.0;
    const Scalar negOne = -1.0;

    // to be Kokkos-ified
    for(auto i=0; i<total; i++)
    {
      if(myGlobalElements[i] == 0)
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i], myGlobalElements[i]+1),
                              tuple(two, negOne));
      else if (myGlobalElements[i] == total - 1)
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i]-1, myGlobalElements[i]),
                              tuple(negOne, two));
      else
        A->insertGlobalValues(myGlobalElements[i],
                              tuple(myGlobalElements[i]-1, myGlobalElements[i], myGlobalElements[i]+1),
                              tuple(negOne, two, negOne));
    }
    A->fillComplete();

    // create the vectors
    RCP<vector_type> b = rcp(new vector_type(map));
    RCP<vector_type> x = rcp(new vector_type(map));
    b->putScalar(0.1);
    x->putScalar(0.1);

    /* Thyra code to link Tpetra with our solver of choice */
    typedef Thyra::TpetraVectorSpace<Scalar, LO, GO, NT> vec_space;
    typedef Thyra::TpetraLinearOp<Scalar, LO, GO, NT> lin_op;
    typedef Thyra::TpetraVector<Scalar, LO, GO, NT> vec;

    RCP<vec_space> thyra_space =
      Thyra::tpetraVectorSpace<Scalar, LO, GO, NT>(map);
    RCP<lin_op> thyra_A =
      Thyra::tpetraLinearOp<Scalar, LO, GO, NT>(thyra_space, thyra_space, A);
    RCP<vec> thyra_x =
      Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, x);
    RCP<vec> thyra_b =
      Thyra::tpetraVector<Scalar, LO, GO, NT>(thyra_space, b);

    typedef Thyra::LinearOpWithSolveFactoryBase<Scalar> opFactory;
    RCP<opFactory> lowsFactory = 
      linearSolverBuilder.createLinearSolveStrategy("");
    lowsFactory->setOStream(f_out);
    lowsFactory->setVerbLevel(Teuchos::VERB_LOW);

    /* Create the linear solver */
    RCP<Thyra::LinearOpWithSolveBase<Scalar> > lows =
      Thyra::linearOpWithSolve<Scalar>(*lowsFactory, thyra_A);

    Thyra::SolveStatus<Scalar> status =
      Thyra::solve<Scalar>(*lows, Thyra::NOTRANS, *thyra_b, thyra_x.ptr());

    out << status << std::endl;
    x->describe(*f_out, Teuchos::VERB_EXTREME);
  }

  return 0;
}
