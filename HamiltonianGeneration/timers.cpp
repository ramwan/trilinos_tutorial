#include "timers.hpp"

Teuchos::RCP<Teuchos::Time> fin_dif_loc_create_timer;
Teuchos::RCP<Teuchos::Time> fin_dif_dis_create_timer;
Teuchos::RCP<Teuchos::Time> atom_grid_create_timer;
Teuchos::RCP<Teuchos::Time> contact_region_create_timer;
Teuchos::Array<Teuchos::RCP<Teuchos::Time>> timers;

void init_timers()
{
  using Teuchos::rcp;
  using Teuchos::Time;
  using Teuchos::TimeMonitor;

  timers();

  fin_dif_loc_create_timer =
    TimeMonitor::getNewCounter("Finite difference local creation timer");
  fin_dif_dis_create_timer =
    TimeMonitor::getNewCounter("Finite difference distributed creation timer");
  atom_grid_create_timer =
    TimeMonitor::getNewCounter("Atom grid creation timer");
  contact_region_create_timer =
    TimeMonitor::getNewCounter("Contact region creation timer");

  // make sure timers are pushed on in the same order as the definitions in 
  // the header file
  timers.push_back(fin_dif_loc_create_timer);
  timers.push_back(fin_dif_dis_create_timer);
  timers.push_back(atom_grid_create_timer);
  timers.push_back(contact_region_create_timer);
}
