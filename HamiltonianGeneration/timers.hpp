#ifndef _PROGRAM_TIMERS_
#define _PROGRAM_TIMERS_

#include <Teuchos_Time.hpp>
#include <Teuchos_TimeMonitor.hpp>
#include <Teuchos_Array.hpp>
#include <Teuchos_RCPDecl.hpp>

#define NUM_TIMERS 2
#define FIN_DIF_LOC_CREATE_TIMER 0
#define FIN_DIF_DIS_CREATE_TIMER 1
#define ATOM_GRID_CREATE_TIMER 2
#define CONTACT_REGION_CREATE_TIMER 3

extern Teuchos::RCP<Teuchos::Time> fin_dif_loc_create_timer;
extern Teuchos::RCP<Teuchos::Time> fin_dif_dis_create_timer;
extern Teuchos::RCP<Teuchos::Time> atom_grid_create_timer;
extern Teuchos::RCP<Teuchos::Time> contact_region_create_timer;

extern Teuchos::Array<Teuchos::RCP<Teuchos::Time>> timers;

void init_timers();
#endif
