#include "problem.hpp"

// Nonmember constructor
Teuchos::RCP<EvaluatorTpetra> evaluatorTpetra
(const Teuchos::RCP<const Teuchos::Comm<int > >& comm,
 Teuchos::RCP<const map_type> xMap,
 Teuchos::RCP<const matrix_type> A,
 Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc)
{
  return Teuchos::rcp(new EvaluatorTpetra
                            (comm, xMap, A, source_loc, drain_loc, gate1_loc,
                             gate2_loc, upper_loc, lower_loc) );
}

// Constructor
EvaluatorTpetra::EvaluatorTpetra
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> xMap,
 Teuchos::RCP<const matrix_type> A,
 Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc) :
 comm_(comm), showGetInvalidArg_(false), source_loc_(source_loc),
 drain_loc_(drain_loc), gate1_loc_(gate1_loc), gate2_loc_(gate2_loc),
 upper_loc_(upper_loc), lower_loc_(lower_loc), A_(A)
{
  typedef Thyra::ModelEvaluatorBase MEB;
  typedef Teuchos::ScalarTraits<Scalar> ST;

  TEUCHOS_ASSERT(nonnull(comm_));

  GO indexBase = 0;
  xOwnedMap_ = Teuchos::rcp(new map_type(*xMap) );
  xSpace_ = Thyra::createVectorSpace<Scalar, LO, GO, NT>(xOwnedMap_);
  
  // residual space
  fOwnedMap_ = xOwnedMap_;
  fSpace_ = xSpace_;

  MEB::InArgsSetup<Scalar> inArgs;
  inArgs.setModelEvalDescription("Finite difference nonlin Poisson");
  inArgs.setSupports(MEB::IN_ARG_x);
  prototypeInArgs_ = inArgs;

  MEB::OutArgsSetup<Scalar> outArgs;
  outArgs.setModelEvalDescription("Finite difference nonlin Poisson");
  outArgs.setSupports(MEB::OUT_ARG_f);
  outArgs.setSupports(MEB::OUT_ARG_W_op);
  prototypeOutArgs_ = outArgs;

  nominalValues_ = inArgs;
}

// Initializers/Accessors

void EvaluatorTpetra::setShowGetInvalidArgs(bool showGetInvalidArg)
{
  showGetInvalidArg_ = showGetInvalidArg;
}

// Public functions overridden from ModelEvaluatory

Teuchos::RCP<const Thyra::VectorSpaceBase<Scalar> >
EvaluatorTpetra::get_x_space() const
{
  return xSpace_;
}

Teuchos::RCP<const Thyra::VectorSpaceBase<Scalar> >
EvaluatorTpetra::get_f_space() const
{
  return fSpace_;
}

Thyra::ModelEvaluatorBase::InArgs<Scalar>
EvaluatorTpetra::getNominalValues() const
{
  return nominalValues_;
}

Thyra::ModelEvaluatorBase::InArgs<Scalar>
EvaluatorTpetra::createInArgs() const
{
  return prototypeInArgs_;
}

// Private functions overridden from ModelEvaluatorDefaultBase

Thyra::ModelEvaluatorBase::OutArgs<Scalar>
EvaluatorTpetra::createOutArgsImpl() const
{
  return prototypeOutArgs_;
}

Scalar EvaluatorTpetra::consolidate_coords
(const Scalar x, const Scalar y, const Scalar z) const
{
  return (x/a - 1)*numz*numy + (y/a - 1)*numz + (z/a - 1);
}

void EvaluatorTpetra::setRegion
(Teuchos::RCP<vector_type> P, Teuchos::RCP<const multi_vec_type> region,
 Scalar value) const
{
  using Teuchos::RCP;

  auto regionView = region->get2dView();
  RCP<const map_type> map = P->getMap();
  Scalar consol, x, y, z;

  for(auto i=0; i<=region->getMap()->getMaxLocalIndex(); i++)
  {
    x = regionView[0][i];
    y = regionView[1][i];
    z = regionView[2][i];

    consol = consolidate_coords(x, y, z);
    auto local = map->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      P->replaceGlobalValue(consol, value);
    }
  }
}

/* Set regionP in vector P to the values in the same region but of vector B.
 * Assumption here is that multi_vec_type is actually a single vector. :)
 */
void EvaluatorTpetra::setRegion
(Teuchos::RCP<vector_type> P, Teuchos::RCP<vector_type> B,
 Teuchos::RCP<const multi_vec_type> region) const
{
  using Teuchos::RCP;

  auto regionView = region->get2dView();
  auto localBView = B->get2dView();
  RCP<const map_type> mapP = P->getMap();
  RCP<const map_type> mapB = B->getMap();

  GO consol;
  Scalar x, y, z;

  for(auto i=0; i<=region->getMap()->getMaxLocalIndex(); i++)
  {
    x = regionView[0][i];
    y = regionView[1][i];
    z = regionView[2][i];

    consol = consolidate_coords(x, y, z);
    // if our coordinate is contained in mapB, it's also contained in mapP
    // as my specific assumption here is that size(B) == size(P) and their maps
    // are the same. this is GENERALLY not the case but here it is the case.
    auto local = mapB->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      auto value = localBView[0][local];
      P->replaceGlobalValue(consol, value);
    }
  }
}

/* We should be returning a multivector that's actually just a single column */
Teuchos::RCP<vector_type>
EvaluatorTpetra::residualEval(Teuchos::RCP<vector_type> P) const
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::null;

  auto density_SMALL = [=](const Scalar V, Scalar Nc, Scalar np)
    {
      // we need to do error handling for the gsl functions to set underflows
      // to 0.
      gsl_sf_result eval;
      Scalar nFx = V / kT;
      gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off();
      int ret = gsl_sf_fermi_dirac_half_e(nFx, &eval);
      gsl_set_error_handler(old_error_handler);

      Scalar total_den;
      if (ret == GSL_SUCCESS)
        total_den = (Nc * eval.val) - np;
      else
        total_den = -np;

      return total_den;
    };
  
  setRegion(P, this->upper_loc_, -1.13);
  setRegion(P, this->lower_loc_, -1.13);

  RCP<const map_type> Pmap = P->getMap();
  RCP<vector_type> B = rcp(new vector_type(Pmap, 1));

  // B = density_SMALL(P, NcSi, 0) * a_squared_on_eps_si
  const Scalar a2_epssi =  a*a/eps_si;
  Scalar val = 0;
  auto P_view = P->get2dViewNonConst();
  // setting B
  Kokkos::RangePolicy<> range(0, Pmap->getMaxLocalIndex() + 1);

  Kokkos::parallel_for("fill B", range,
    KOKKOS_LAMBDA(const LO l)
    {
      Scalar val = density_SMALL(P_view[0][l], NcSi, 0) * a2_epssi;
      B->replaceLocalValue(l, val);
    });

  // setting contact regions in B
  LO local;
  Scalar x, y, z;
  GO consol;

  // Note for the future: here I didn't specify anything about execution and
  // memory spaces. If we want to start using CUDA space, then we will have to
  // start making sure that host and execution space memory is synced when we
  // start doing multiprocessor operations... something like that.

  // source 
  auto sourceview = this->source_loc_->get2dView();
  for(auto l=0; l<sourceview[0].size(); l++)
  {
    x = sourceview[0][l];
    y = sourceview[1][l];
    z = sourceview[2][l];
    consol = consolidate_coords(x, y, z);
    local = Pmap->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      val = density_SMALL(P_view[0][local], NcSD, NpSD) * a2_epssi;
      B->replaceGlobalValue(consol, val);
    }
  }
  sourceview = null;

  // drain
  auto drainview = this->drain_loc_->get2dView();
  for(auto l=0; l<drainview[0].size(); l++)
  {
    x = drainview[0][l];
    y = drainview[1][l];
    z = drainview[2][l];
    consol = consolidate_coords(x, y, z);
    local = Pmap->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      val = density_SMALL(P_view[0][local], NcSD, NpSD) * a2_epssi;
      B->replaceGlobalValue(consol, val);
    }
  }
  drainview = null;

  // gate1
  auto gate1view = this->gate1_loc_->get2dView();
  for(auto l=0; l<gate1view[0].size(); l++)
  {
    x = gate1view[0][l];
    y = gate1view[1][l];
    z = gate1view[2][l];
    consol = consolidate_coords(x, y, z);
    local = Pmap->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      val = density_SMALL(P_view[0][local], NcSD, NpSD) * a2_epssi;
      B->replaceGlobalValue(consol, val);
    }
  }
  gate1view = null;

  // gate2
  auto gate2view = this->gate2_loc_->get2dView();
  for(auto l=0; l<gate2view[0].size(); l++)
  {
    x = gate2view[0][l];
    y = gate2view[1][l];
    z = gate2view[2][l];
    consol = consolidate_coords(x, y, z);
    local = Pmap->getLocalElement(consol);

    if ( local != Teuchos::OrdinalTraits<LO>::invalid() )
    {
      val = density_SMALL(P_view[0][local], NcSD, NpSD) * a2_epssi;
      B->replaceGlobalValue(consol, val);
    }
  }
  gate2view = null;

  RCP<vector_type> temp = rcp(new vector_type(Pmap, 1));
  this->A_->apply(*P, *temp);

  setRegion(temp, B, this->upper_loc_);
  setRegion(temp, B, this->lower_loc_);

  temp->update(-1, *B, Teuchos::ScalarTraits<Scalar>::one());

  return temp;
}

void EvaluatorTpetra::
evalModelImpl(const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
              const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs)
const {
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::rcpFromRef;
  typedef Tpetra::Operator<Scalar, LO, GO, NT> tpetra_op;
  typedef Thyra::TpetraOperatorVectorExtraction<Scalar, LO, GO, NT>
    tpetra_extract;

  TEUCHOS_ASSERT(nonnull(inArgs.get_x()));

  const Teuchos::RCP<thyra_vec> f_out = outArgs.get_f();
  Teuchos::RCP<thyra_vec> x_in = Teuchos::rcp_const_cast<thyra_vec>( inArgs.get_x() );

  const bool fill_f = nonnull(f_out);

  if ( fill_f )
  {
    // get the underlying tpetra objects
    RCP<vector_type> f = tpetra_extract::getTpetraVector(f_out);
    f->putScalar(0.0);
    RCP<vector_type> P = tpetra_extract::getTpetraVector(x_in);

    RCP<vector_type> res = residualEval(P);

    auto myGlobalElements = f->getMap()->getMyGlobalIndices();
    auto residView = res->getData();
    Kokkos::parallel_for("copy into f", myGlobalElements.size(),
      KOKKOS_LAMBDA(const size_t i)
        {
          f->replaceGlobalValue(myGlobalElements[i], residView[i]);
        });
  }
}
