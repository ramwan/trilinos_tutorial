echo "Uniform grid spacing testing."
python3 neumann_test.py 1 1 1 0.5 0.5 0.5
python3 neumann_test.py 1 1 1 0.3 0.3 0.3
python3 neumann_test.py 1 1 1 0.1 0.1 0.1
#echo "Non-uniform grid spacing testing."
#python3 neumann_test.py 1 1 1 0.1 0.2 0.3
#python3 neumann_test.py 10 10 3 0.14 0.12 0.44
#python3 neumann_test.py 26 26 3 0.11 0.34 0.113
