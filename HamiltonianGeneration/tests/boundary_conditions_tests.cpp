#include "test_assert.hpp"
#include "../boundary_conditions.hpp"

void test_Three_P_Cart()
{
  Three_P_Cart p(10, 10, 10);
  UNIT_ASSERT(p._3x() == 10);
  UNIT_ASSERT(p._3y() == 10);
  UNIT_ASSERT(p._3z() == 10);

  Three_P_Cart q(10, 10, 10);
  UNIT_ASSERT(p == q);
}

void test_Three_Cartesian()
{
  using Teuchos::rcp;

  // x-y square from -1,-1 to 1 1
  // z dimensions from 0 to 1
  // 4 points in each dimension
  Three_Cartesian d(-1, -1, 0, 1, 1, 1, 0.5, 0.5, 0.25);
  UNIT_ASSERT(d._3xmin() == -1);
  UNIT_ASSERT(d._3ymin() == -1);
  UNIT_ASSERT(d._3zmin() == 0);
  UNIT_ASSERT(d._3xmax() == 1);
  UNIT_ASSERT(d._3ymax() == 1);
  UNIT_ASSERT(d._3zmax() == 1);
  UNIT_ASSERT(d._3xlen() == 2);
  UNIT_ASSERT(d._3ylen() == 2);
  UNIT_ASSERT(d._3zlen() == 1);
  UNIT_ASSERT(d._3xstep() == 0.5);
  UNIT_ASSERT(d._3ystep() == 0.5);
  UNIT_ASSERT(d._3zstep() == 0.25);
  UNIT_ASSERT(d.total_points() == 64);

  UNIT_ASSERT(*d.globalOrdinalToPoint(0) == Three_P_Cart(0, 0, 0));
  UNIT_ASSERT(*d.globalOrdinalToPoint(1) == Three_P_Cart(0, 1, 0));
  UNIT_ASSERT(*d.globalOrdinalToPoint(2) == Three_P_Cart(0, 2, 0));
  UNIT_ASSERT(*d.globalOrdinalToPoint(3) == Three_P_Cart(0, 3, 0));
  UNIT_ASSERT(*d.globalOrdinalToPoint(5) == Three_P_Cart(1, 1, 0));
  UNIT_ASSERT(*d.globalOrdinalToPoint(63) == Three_P_Cart(3, 3, 3));
  UNIT_ASSERT(d.pointToGlobalOrdinal(rcp(new Three_P_Cart (0, 3, 0))) == 3);
  UNIT_ASSERT(d.pointToGlobalOrdinal(rcp(new Three_P_Cart (1, 1, 0))) == 5);
  UNIT_ASSERT(d.pointToGlobalOrdinal(rcp(new Three_P_Cart (3, 3, 3))) == 63);
}

// void test_NeumannCond() {}; // this test is done externally from python

int main()
{
  test_Three_P_Cart();
  test_Three_Cartesian();
  return 0;
}
