cpp_res = 0
with open('./cpp_solution_vector2.out', 'r') as f:
    for line in f:
        cpp_res += float(line)

python_res = 0
with open('./python_solution_vector.out', 'r') as f:
    for line in f:
        python_res += float(line)

print("CPP abs sum: ", str(cpp_res))
print("Python abs sum: ", str(python_res))
print("Diff: ", str(abs(cpp_res - python_res)))
