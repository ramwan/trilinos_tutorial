#include <iostream>
#include <stdexcept>
#include <string>

#define UNIT_ASSERT( condition )                                              \
{                                                                             \
  if ( !(condition) )                                                         \
  {                                                                           \
    std::cout << std::string( __FILE__ )                                      \
      + std::string( ":" )                                                    \
      + std::to_string( __LINE__ )                                            \
      + std::string( " in " )                                                 \
      + std::string( __FUNCTION__ )                                           \
      << std::endl;                                                           \
    throw std::runtime_error("");                                             \
  }                                                                           \
}
