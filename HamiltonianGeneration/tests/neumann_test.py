import numpy as np
import multiprocessing as mp
import ctypes
import sys
import subprocess
import re
from ast import literal_eval
from scipy import sparse as sp
from math import floor

# Differential Matrix A
lx = float(sys.argv[1])
ly = float(sys.argv[2])
lz = float(sys.argv[3])
stepx = float(sys.argv[4])
stepy = float(sys.argv[5])
stepz = float(sys.argv[6])

Nx = floor(lx/stepx)
Ny = floor(ly/stepy)
Nz = floor(lz/stepz)
no_of_atoms = Nx * Ny * Nz
Nyz = Ny * Nz

# generate main diagonal including Neumann boundary condition
on = -6 * np.ones(no_of_atoms)
for i in range(no_of_atoms):
    max = 3
    adjust = 0
    
    # condition for middle strips in the x-y plane
    if (Nyz <= i) and (i <= no_of_atoms - 1 - Nyz):
        adjust = adjust + 1
    # condition for other middle strips in x-y plane
    if (i%Nyz >= Nz) and (i%Nyz < (Nyz - Nz)):
        adjust = adjust + 1
    # condition for differentiating top and bottom z planes
    if (i%Nz != 0) and (i%Nz != (Nz-1)):
        adjust = adjust + 1

    on[i] = on[i] + (max - adjust)

off1 = np.ones(no_of_atoms - Nyz) # outtermost off-diag elements

off2 = np.tile(np.append(np.ones(Nz*(Ny-1)), np.zeros(Nz)), Nx) # middle off-diag elements
off2 = off2[:(no_of_atoms-Nz)]

off3 = np.tile(np.append(np.ones(Nz-1), 0), Nx*Ny) # innermost off-diag elements
off3 = off3[:(no_of_atoms-1)]

# Sparse Storage
new_diagonals = [on, off1, off1,
                 off2, off2, off3, off3]
offsets = [0, Nyz, -Nyz, Nz, -Nz, 1, -1]
correct_mat = sp.csr_matrix(sp.diags(new_diagonals, offsets))

# compare with matrices built via cpp code
t = subprocess.run(["/scratch/ad73/rw6126/trilinos_tutorials/bin/generate_matrix",\
                    "--x1=0", "--x2="+str(lx), "--xstep="+str(stepx),
                    "--y1=0", "--y2="+str(ly), "--ystep="+str(stepy),
                    "--z1=0", "--z2="+str(lz), "--zstep="+str(stepz),
                    "--debug=1"],
                    capture_output=True)
decode_t = t.stdout.decode("utf-8")

keep = False
row = 0
for line in decode_t.splitlines():
    if keep:
        if "Starting the solve." in line:
            keep = False
        t = re.findall(r"\(.*?\)", line)
        if t is not None:
            for elem in t:
                evaluation = literal_eval(elem)
                if correct_mat[row, evaluation[0]] != evaluation[1]:
                    raise ValueError("row: "+str(row)+", col: "+\
                                      str(evaluation[0])+" has value "+\
                                      str(evaluation[1])+" which is wrong.")
        row += 1

    if "Entries(Index,Value)" in line:
        keep = True

print("TEST PASSED!")
