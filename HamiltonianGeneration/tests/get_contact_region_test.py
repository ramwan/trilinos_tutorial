import numpy as np
import multiprocessing as mp
import ctypes
import sys
import subprocess
import re
from ast import literal_eval
from scipy import sparse as sp
from math import floor

# Differential Matrix A
lx = 10
ly = 10
lz = 10
a = 0.56

nx = floor(lx/a)
ny = floor(ly/a)
nz = floor(lz/a)

def atom_grid():
    raw_x = np.linspace(a, a*nx, num=nx, endpoint=True)
    raw_y = np.linspace(a, a*ny, num=ny, endpoint=True)
    raw_z = np.linspace(a, a*nz, num=nz, endpoint=True)
    grid_x, grid_y, grid_z = np.meshgrid(raw_x, raw_y, raw_z, indexing='ij')

    sx = np.prod(grid_x.shape)
    sy = np.prod(grid_y.shape)
    sz = np.prod(grid_z.shape)

    x = np.reshape(grid_x, (sx,))
    y = np.reshape(grid_y, (sy,))
    z = np.reshape(grid_z, (sz,))
    
    atom = np.c_[x, y, z]
    return atom

def get_contact_region(contact, atom):
    x1 = contact[0][0]
    x2 = contact[1][0]
    y1 = contact[0][1]
    y2 = contact[1][1]
    z1 = contact[0][2]
    z2 = contact[1][2]

    region = []
    a = atom[0, 0]
    for i in range(atom.shape[0]):
        region_x = atom[i, 0]
        region_y = atom[i, 1]
        region_z = atom[i, 2]
        if (region_x >= x1-a/2 and region_x <= x2+a/2):
            if (region_y >= y1-a/2 and region_y <= y2+a/2):
                if (region_z >= z1-a/2 and region_z <= z2+a/2):
                    region.append(i)
    return np.array(region)

atoms = atom_grid()
sample_contact_1 = np.array([[0, 0, 0], [1, 1, 1]])
region_1 = get_contact_region(sample_contact_1, atoms)

# compare with matrices built via cpp code
t = subprocess.run(["/scratch/ad73/rw6126/trilinos_tutorials/bin/generate_matrix",\
                    "--x1=0", "--x2="+str(lx), "--xstep="+str(a),
                    "--y1=0", "--y2="+str(ly), "--ystep="+str(a),
                    "--z1=0", "--z2="+str(lz), "--zstep="+str(a),
                    "--debug=2"],
                    capture_output=True)
decode_t = t.stdout.decode("utf-8")
print(decode_t)

'''
keep = False
row = 0
for line in decode_t.splitlines():
    if keep:
        t = re.findall(r"\(.*?\)", line)
        if t is not None:
            for elem in t:
                evaluation = literal_eval(elem)
                if correct_mat[row, evaluation[0]] != evaluation[1]:
                    raise ValueError("row: "+str(row)+", col: "+\
                                      str(evaltuation[0])+" has value "+\
                                      evaluation[1]+" but should have value "+\
                                      str(correct_mat[row, evaluation[0]]))
        row += 1

    if "Entries(Index,Value)" in line:
        keep = True

print("TEST PASSED!")
'''
