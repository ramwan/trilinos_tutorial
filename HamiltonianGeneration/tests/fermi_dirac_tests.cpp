#include <iostream>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_sf_fermi_dirac.h>

double kT = 1.38e-23 * 100e-3;
double Nc = 1.7197e-7; // NcSi
double np = 0;

double densitySmall(const double V, const double Nc, const double np)
{
  gsl_sf_result eval;
  double nFx = V / kT;
  gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off();
  int ret = gsl_sf_fermi_dirac_half_e(nFx, &eval);
  gsl_set_error_handler(old_error_handler);

  double total_den;
  if (ret == GSL_SUCCESS)
    total_den = (Nc * eval.val) - np;
  else
    total_den = -np;
  
  return total_den;
}

int main()
{
  std::cout << "V: -0.3, -- " << densitySmall(-0.3, Nc, np) << std::endl;
  std::cout << "V: -0.29, -- " << densitySmall(-0.29, Nc, np) << std::endl;
  std::cout << "V: -0.27, -- " << densitySmall(-0.27, Nc, np) << std::endl;
  std::cout << "V: -0.24, -- " << densitySmall(-0.24, Nc, np) << std::endl;
  std::cout << "V: -0.20, -- " << densitySmall(-0.20, Nc, np) << std::endl;

  gsl_sf_result eval;
  gsl_error_handler_t *old_error_handler = gsl_set_error_handler_off();
  int ret = gsl_sf_fermi_dirac_half_e(0.26/kT, &eval);
  gsl_set_error_handler(old_error_handler);
  double val;
  if (ret == GSL_SUCCESS)
    val = eval.val;
  else
    val = 0;

  std::cout << "V: 0.26, -- " << densitySmall(0.26, 0.8/val, 1.7/1.5) << std::endl;

  std::cout << 0.8 / gsl_sf_fermi_dirac_half(0.26/kT) << std::endl;
  std::cout << 0.83 / gsl_sf_fermi_dirac_half(0.278/kT) << std::endl;
  std::cout << 0.84 / gsl_sf_fermi_dirac_half(0.286/kT) << std::endl;

  return 0;
}
