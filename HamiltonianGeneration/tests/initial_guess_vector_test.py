import numpy as np
import multiprocessing as mp
import ctypes
import sys
import subprocess
import re
from ast import literal_eval
from scipy import sparse as sp
from math import floor

lx = 56.0
ly = 128.0
lz = 128.0
a = 0.56
stepx = a
stepy = a
stepz = a

Nx = floor(lx/stepx)
Ny = floor(ly/stepy)
Nz = floor(lz/stepz)
no_of_atoms = Nx * Ny * Nz

def get_contact_region(contact, atom):
    x1 = contact[0][0]
    x2 = contact[1][0]
    y1 = contact[0][1]
    y2 = contact[1][1]
    z1 = contact[0][2]
    z2 = contact[1][2]

    region = []
    a = atom[0, 0]
    for i in range(atom.shape[0]):
        region_x = atom[i, 0]
        region_y = atom[i, 1]
        region_z = atom[i, 2]
        if (region_x >= x1-a/2 and region_x <= x2+a/2):
            if (region_y >= y1-a/2 and region_y <= y2+a/2):
                if (region_z >= z1-a/2 and region_z <= z2+a/2):
                    region.append(i)

    return np.array(region)

def atom_grid():
    raw_x = np.linspace(a, a*Nx, num=Nx, endpoint=True)
    raw_y = np.linspace(a, a*Ny, num=Ny, endpoint=True)
    raw_z = np.linspace(a, a*Nz, num=Nz, endpoint=True)
    grid_x, grid_y, grid_z = np.meshgrid(raw_x, raw_y, raw_z, indexing='ij')

    sx = np.prod(grid_x.shape)
    sy = np.prod(grid_y.shape)
    sz = np.prod(grid_z.shape)

    x = np.reshape(grid_x, (sx,))
    y = np.reshape(grid_y, (sy,))
    z = np.reshape(grid_z, (sz,))

    atom = np.c_[x, y, z]
    print(str(atom[0, 0]) + ", " + str(atom[0, 1]) + ", " + str(atom[0, 2]))
    print(str(atom[1, 0]) + ", " + str(atom[1, 1]) + ", " + str(atom[1, 2]))
    print(str(atom[2, 0]) + ", " + str(atom[2, 1]) + ", " + str(atom[2, 2]))
    print(str(atom[-1, 0]) + ", " + str(atom[-1, 1]) + ", " + str(atom[-1, 2]))
    return atom

atom = atom_grid()
print("Nx: ", Nx)
print("Ny: ", Ny)
print("Nz: ", Nz)
print("Atoms length: " + str(len(atom)))

z_start = lz/2
x_min = a
y_min = a
z_min = a
x_max = atom[-1, 0]
y_max = atom[-1, 1]
z_max = atom[-1, 2]
source = np.array([[x_min, 61, z_start-0.75], [19, 67, z_start+0.75]])
drain = np.array([[37, 61, z_start-0.75], [x_max, 67, z_start+0.75]])
gate1 = np.array([[27, y_min, z_start-0.75], [39, 10, z_start+0.75]])
gate2 = np.array([[19, 119, z_start-0.75], [39, y_max+2, z_start+0.75]])
lower_limit = np.array([[x_min, y_min, z_min], [x_max, y_max, z_min]])
upper_limit = np.array([[x_min, y_min, z_max], [x_max, y_max, z_max]])

source_region_indx = get_contact_region(source, atom)
'''
drain_region_indx = get_contact_region(drain, atom)
gate1_region_indx = get_contact_region(gate1, atom)
gate2_region_indx = get_contact_region(gate2, atom)
upper_region_indx = get_contact_region(upper_limit, atom)
lower_region_indx = get_contact_region(lower_limit, atom)

X_init = -0.3 * np.ones(no_of_atoms)
X_init[source_region_indx] = 0.26
X_init[drain_region_indx] = 0.26
X_init[gate1_region_indx] = 0.278
X_init[gate2_region_indx] = 0.286
'''

'''
print("source: " + str(len(source_region_indx)))
print("drain: " + str(len(drain_region_indx)))
print("gate1: " + str(len(gate1_region_indx)))
print("gate2: " + str(len(gate2_region_indx)))
print("upper: " + str(len(upper_region_indx)))
print("lower: " + str(len(lower_region_indx)))
'''

#for i in X_init:
#    print(i)
