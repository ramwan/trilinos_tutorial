#ifndef _NONLIN_INTERFACE_
#define _NONLIN_INTERFACE_

#include "data_types.hpp"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_sf_result.h>
#include <gsl/gsl_sf_fermi_dirac.h>

#include <Kokkos_Core.hpp>

#include <Thyra_DefaultSpmdVectorSpace.hpp>
#include <Thyra_DefaultSerialDenseLinearOpWithSolveFactory.hpp>
#include <Thyra_DetachedMultiVectorView.hpp>
#include <Thyra_DetachedVectorView.hpp>
#include <Thyra_MultiVectorStdOps.hpp>
#include <Thyra_VectorStdOps.hpp>
#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_StateFuncModelEvaluatorBase.hpp>

#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_MultiVector_decl.hpp>
#include <Tpetra_CrsMatrix.hpp>
#include <Teuchos_RCPDecl.hpp>

class EvaluatorTpetra;

// Nonmember constructor
Teuchos::RCP<EvaluatorTpetra> evaluatorTpetra
(const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const map_type> xMap,
 Teuchos::RCP<const matrix_type> A,
 Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc);

// Nonlinear Poisson evaluator extension class
class EvaluatorTpetra : public Thyra::StateFuncModelEvaluatorBase<Scalar>
{
public:
  typedef Thyra::VectorBase<Scalar> thyra_vec;
  typedef Thyra::VectorSpaceBase<Scalar> thyra_vec_space;

  // Constructor
  EvaluatorTpetra
    (const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
     Teuchos::RCP<const map_type> xMap,
     Teuchos::RCP<const matrix_type> A,
     Teuchos::RCP<const multi_vec_type> source_loc,
     Teuchos::RCP<const multi_vec_type> drain_loc,
     Teuchos::RCP<const multi_vec_type> gate1_loc,
     Teuchos::RCP<const multi_vec_type> gate2_loc,
     Teuchos::RCP<const multi_vec_type> upper_loc,
     Teuchos::RCP<const multi_vec_type> lower_loc);

  // Initializers/Accessors
  void setShowGetInvalidArgs(bool showGetInvalidArg);

  // Public functions overridden from ModelEvaluator
  Teuchos::RCP<const thyra_vec_space> get_x_space() const;
  Teuchos::RCP<const thyra_vec_space> get_f_space() const;
  Thyra::ModelEvaluatorBase::InArgs<Scalar> getNominalValues() const;
   
  Thyra::ModelEvaluatorBase::InArgs<Scalar> createInArgs() const;

private:
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> createOutArgsImpl() const;

  // residual calculation functions
  Scalar consolidate_coords
    (const Scalar x, const Scalar y, const Scalar z) const;
  void setRegion(Teuchos::RCP<vector_type> P,
                 Teuchos::RCP<const multi_vec_type> region,
                 Scalar value) const;
  void setRegion(Teuchos::RCP<vector_type> P,
                 Teuchos::RCP<vector_type> B,
                 Teuchos::RCP<const multi_vec_type> region) const;

  Teuchos::RCP<vector_type>
    residualEval(Teuchos::RCP<vector_type> P) const;
  //

  void evalModelImpl(
    const Thyra::ModelEvaluatorBase::InArgs<Scalar> &inArgs,
    const Thyra::ModelEvaluatorBase::OutArgs<Scalar> &outArgs
    ) const;

private: // data members
  const Teuchos::RCP<const Teuchos::Comm<int> > comm_;

  Teuchos::RCP<const thyra_vec_space> xSpace_;
  Teuchos::RCP<const map_type> xOwnedMap_;
  Teuchos::RCP<const thyra_vec_space> fSpace_;
  Teuchos::RCP<const map_type> fOwnedMap_;

  Thyra::ModelEvaluatorBase::InArgs<Scalar> nominalValues_;
  bool showGetInvalidArg_;
  Thyra::ModelEvaluatorBase::InArgs<Scalar> prototypeInArgs_;
  Thyra::ModelEvaluatorBase::OutArgs<Scalar> prototypeOutArgs_;

  // Extra data for calculating residual.
  // Our locations are multivectors of size 3 x n (for an xyz coord system)
  Teuchos::RCP<const matrix_type> A_;
  Teuchos::RCP<const multi_vec_type> source_loc_;
  Teuchos::RCP<const multi_vec_type> drain_loc_;
  Teuchos::RCP<const multi_vec_type> gate1_loc_;
  Teuchos::RCP<const multi_vec_type> gate2_loc_;
  Teuchos::RCP<const multi_vec_type> upper_loc_;
  Teuchos::RCP<const multi_vec_type> lower_loc_;
};

#endif
