#ifndef _SOLVER_
#define _SOLVER_ 

#include "data_types.hpp"
#include "problem.hpp"
#include "timers.hpp"

#include <gsl/gsl_sf_fermi_dirac.h>
// used for linear solver, could probably remove these
#include <BelosPseudoBlockGmresSolMgr.hpp>
#include <BelosTpetraAdapter.hpp>
#include <BelosSolverFactory.hpp>
//
#include <BelosTypes.hpp>
#include <Kokkos_Core.hpp>

#include <NOX.H>
#include <NOX_MatrixFree_ModelEvaluatorDecorator.hpp>
#include <NOX_Thyra.H>
#include <NOX_Thyra_MatrixFreeJacobianOperator.hpp>

#include <Stratimikos_DefaultLinearSolverBuilder.hpp>

#include <Teuchos_Tuple.hpp>
#include <Teuchos_ParameterList.hpp>

#include <Thyra_TpetraThyraWrappers.hpp>
#include <Thyra_LinearOpWithSolveFactoryHelpers.hpp>

#include <Tpetra_Core.hpp>
#include <Tpetra_Vector.hpp>
#include <Tpetra_CrsMatrix.hpp>

template<typename T> Kokkos::View<Scalar*>
linspace(T start_in, T end_in, Scalar num_in, bool include_end=true);

// local grid creation
Teuchos::RCP<const multi_vec_type>
atomGrid(const Teuchos::RCP<const Teuchos::Comm<int> > comm,
         Scalar xtotal, Scalar ytotal, Scalar ztotal,
         std::ostream& out);

// local vector creation
Teuchos::RCP<const multi_vec_type>
getContactRegion
(Teuchos::RCP<const multi_vec_type> atoms,
 const Scalar contact[2][3],
 const Scalar xstep, const Scalar ystep, const Scalar zstep,
 const Teuchos::RCP<const Teuchos::Comm<int> > comm);

// distributed vector
Teuchos::RCP<vector_type>
build_initial_guess
(Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 const Scalar xtotal, const Scalar ytotal, const Scalar ztotal,
 const Teuchos::RCP<const Teuchos::Comm<int> > comm);

Teuchos::RCP<vector_type>
nonlinear_solve
(Teuchos::RCP<vector_type> initial_guess,
 Teuchos::RCP<const matrix_type> A,
 const Teuchos::RCP<const Teuchos::Comm<int> >& comm,
 Teuchos::RCP<const multi_vec_type> source_loc,
 Teuchos::RCP<const multi_vec_type> drain_loc,
 Teuchos::RCP<const multi_vec_type> gate1_loc,
 Teuchos::RCP<const multi_vec_type> gate2_loc,
 Teuchos::RCP<const multi_vec_type> upper_loc,
 Teuchos::RCP<const multi_vec_type> lower_loc,
 const Scalar xtotal, const Scalar ytotal, const Scalar ztotal,
 std::ostream& out);

#endif
