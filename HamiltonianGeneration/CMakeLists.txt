INCLUDE_DIRECTORIES ( ./ ${Trilinos_INCLUDE_DIRS} ${Trilinos_TPL_INCLUDE_DIRS} )

LINK_DIRECTORIES (${Trilinos_LIBRARY_DIRS} ${Trilinos_TPL_LIBRARY_DIRS})

ADD_LIBRARY(timers OBJECT timers.cpp)
ADD_LIBRARY(finite_diff OBJECT finite_difference_matrix.cpp)
ADD_LIBRARY(data_types OBJECT data_types.cpp)
ADD_LIBRARY(solver OBJECT solver.cpp)
ADD_LIBRARY(problem OBJECT problem.cpp problem.hpp)

ADD_EXECUTABLE(nonlin_solver_debug
               main.cpp
               $<TARGET_OBJECTS:timers>
               $<TARGET_OBJECTS:finite_diff>
               $<TARGET_OBJECTS:problem>
               $<TARGET_OBJECTS:solver>
               $<TARGET_OBJECTS:data_types>)
TARGET_LINK_LIBRARIES(nonlin_solver_debug
                      ${Trilinos_LIBRARIES}
                      ${Trilinos_TPL_LIBRARIES})

ENABLE_TESTING()
#ADD_SUBDIRECTORY(tests)
