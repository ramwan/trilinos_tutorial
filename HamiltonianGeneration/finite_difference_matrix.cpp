#include "finite_difference_matrix.hpp"

void globalOrdinalToPoint(const GO g, GO point[3])
{
  GO x = g/(numy * numz);
  GO layer = g - x*numy*numz;
  GO y = layer/numz;
  GO z = layer%numz;
  
  point[0] = x; point[1] = y; point[2] = z;
}

GO pointToGlobalOrdinal(const GO point[3])
{
  return point[2] + point[0]*numy*numz + point[1]*numz;
}

Teuchos::RCP<matrix_type> create_distributed
(const Teuchos::RCP<const Teuchos::Comm<int> >&comm,
 Teuchos::RCP<map_type> global_map,
 const bool serial_node)
{
  using Teuchos::RCP;
  using Teuchos::rcp;
  using Teuchos::Tuple;
  using Teuchos::tuple;
  using Teuchos::Array;
  using Teuchos::ArrayView;

  Teuchos::TimeMonitor local_timer( *timers[FIN_DIF_DIS_CREATE_TIMER] );

  const size_t myRank = comm->getRank();
  int ents_per_row = 7;
  const GO num_points = numx*numy*numz;

  RCP<matrix_type> A(new matrix_type(global_map, ents_per_row));

  const Scalar d = static_cast<Scalar>(-6.0);
  const Scalar od = static_cast<Scalar>(1.0);
  Scalar boundary_modifier;

  // helper lambdas
  RCP<Array<GO> > rowIndices = rcp(new Array<GO>());
  RCP<Array<Scalar> > values = rcp(new Array<Scalar>());
  auto getRowValues = [=] (const GO g)
    {
      rowIndices->clear(); values->clear();
      GO point[3] = {0, 0, 0};
      GO tmp[3] = {0, 0, 0};
      globalOrdinalToPoint(g, point);
      auto x = point[0]; auto y = point[1]; auto z = point[2];

      Scalar boundaryModifier = 0;
      if (x <= 0 || x >= numx-1) ++boundaryModifier;
      if (y <= 0 || y >= numy-1) ++boundaryModifier;
      if (z <= 0 || z >= numz-1) ++boundaryModifier;

      rowIndices->push_back(g); // push on diagonal element
      values->push_back(d + boundaryModifier);

      if (x+1 < numx)
      {
        tmp[0] = point[0]+1; tmp[1] = point[1]; tmp[2] = point[2];
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
      if (x-1 >= 0)
      {
        tmp[0] = point[0]-1; tmp[1] = point[1]; tmp[2] = point[2];
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
      if (y+1 < numy)
      {
        tmp[0] = point[0]; tmp[1] = point[1]+1; tmp[2] = point[2];
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
      if (y-1 >= 0)
      {
        tmp[0] = point[0]; tmp[1] = point[1]-1; tmp[2] = point[2];
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
      if (z+1 < numz)
      {
        tmp[0] = point[0]; tmp[1] = point[1]; tmp[2] = point[2]+1;
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
      if (z-1 >= 0)
      {
        tmp[0] = point[0]; tmp[1] = point[1]; tmp[2] = point[2]-1;
        rowIndices->push_back( pointToGlobalOrdinal(tmp) );
        values->push_back( od );
      }
    };

  if(serial_node)
  {
    for(LO l=0; l<=global_map->getMaxLocalIndex(); ++l)
    {
      const GO g = global_map->getGlobalElement(l);
      getRowValues(g);
      A->insertGlobalValues(g, *rowIndices, *values);
    }
  } else
  {
    LO maxLocalIndex = global_map->getMaxLocalIndex();
    LO localNumElems = global_map->getNodeNumElements();

    // arbitrary specification
    int leagueSize = 4;
    Kokkos::TeamPolicy<> policy(leagueSize, Kokkos::AUTO);
    typedef Kokkos::TeamPolicy<>::member_type memberType;

    Kokkos::parallel_for("boundary conditions setting", policy,
      KOKKOS_LAMBDA(memberType teamMember)
        {
          // calculate thread ID
          int k = teamMember.league_rank() * teamMember.team_size() +
                  teamMember.team_rank();
          k += myRank * (leagueSize * teamMember.team_size());

          int totalThreads = leagueSize * teamMember.team_size();
          GO setSize = localNumElems / totalThreads;
          GO globalRowBase = k * setSize;

          // since we probably won't have an even divide, put all the remaining
          // work in the last team member on the last processor
          if (teamMember.league_rank() == leagueSize - 1 &&
              teamMember.team_rank() == teamMember.team_size() - 1 &&
              myRank == (comm->getSize() - 1) )
            setSize += (setSize * k) % totalThreads;

          for(auto i=0; i<setSize; i++)
          {
            if (global_map->isNodeGlobalElement(globalRowBase + i))
            {
              getRowValues(globalRowBase + i);
              A->insertGlobalValues(globalRowBase + i, *rowIndices, *values);
            }
          }
        });
  }
  
  A->fillComplete();
  return A;
}
